package be.crafftit.beerkeeper.model;

/**
 * Created by gizmo on 15/08/14.
 */
public enum BeerType {

    AMBER_ALE, BLONDE_ALE, BROWN_ALE, CHAMPAGNE, DUBBEL, FLEMISH_RED, LAMBIC, SCOTCH_ALE, STOUT, STRONG_ALE, TABLE_BEER, TRIPEL, WHITE, CHRISTMAS

}
