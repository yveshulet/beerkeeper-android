package be.crafftit.beerkeeper.model;

/**
 * Created by gizmo on 15/08/14.
 */
public enum BeerFermentationType {

    WARM, COLD, SPONTANEOUS

}
