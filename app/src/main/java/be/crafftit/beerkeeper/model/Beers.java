package be.crafftit.beerkeeper.model;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by gizmo on 15/08/14.
 */
public final class Beers {

    public static final List<Beer> BEERS = ImmutableList.of(

            new Beer("chimay.blue", "chimay.blue.description", Country.BELGIUM)

    );

    public static final Map<String, Beer> BEERS_MAP = Maps.newHashMap();

    static {

        for (final Beer beer : BEERS) {
            BEERS_MAP.put(beer.getIdentifier(), beer);
        }
    }

}
