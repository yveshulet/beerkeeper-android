package be.crafftit.beerkeeper.model;

import com.google.common.base.Objects;

import java.util.Date;
import java.util.UUID;

/**
 * Created by gizmo on 15/08/14.
 */
public final class Beer {

    private final String identifier;
    private final String title;
    private final String description;
    private final Country country;
    private String opinion;
    private Integer rating;
    private Date lastCheckingDate;
    private String lastCheckingBar;


    public Beer(final String title, final String description, final Country country) {
        this.identifier = UUID.randomUUID().toString();
        this.title = title;
        this.description = description;
        this.country = country;
    }

    public String getIdentifier() {
        return identifier;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Country getCountry() {
        return country;
    }

    public String getOpinion() {
        return opinion;
    }

    public Integer getRating() {
        return rating;
    }

    public Date getLastCheckingDate() {
        if (lastCheckingDate != null) {
            return (Date) lastCheckingDate.clone();
        }
        return null;
    }

    public String getLastCheckingBar() {
        return lastCheckingBar;
    }

    public void setOpinion(final String opinion) {
        this.opinion = opinion;
    }

    public void setRating(final Integer rating) {
        this.rating = rating;
    }

    public void setLastCheckingDate(final Date lastCheckingDate) {
        this.lastCheckingDate = lastCheckingDate;
    }

    public void setLastCheckingBar(final String lastCheckingBar) {
        this.lastCheckingBar = lastCheckingBar;
    }

    @Override
    public String toString() {
        return title;
    }
}
